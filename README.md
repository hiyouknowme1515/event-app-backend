Events  App Api

Technology Used: NodeJs, Express

Database Used: MongoDB,

Specifications:	

1. ` `User can create his/her account and list their events 
1. They can delete their account 
1. Update their post, delete their post
1. On User Profile page their events which they have posted are also shown


Routes

"/events": " get rquest to show all the events",

"/register": " post request to register a user",

"/login": " post request to login a user",

"/user/:userId": " get request to fetch the user whose userId is in Url , put request to update user details , delete request to delete the user",

"/event/new/:userId" : "post request to create new event from a logged in user",

` `"/event/:eventId" : "put request to update the event detalis , delete request to delete the event",


"/events/by/:userId": "get request to show the events by a user"



